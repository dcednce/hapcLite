//*****************************************************************************
// CCAnalysis.java
//*****************************************************************************
package edu.utah.med.genepi.stat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.SortedSet;
import java.util.TreeSet;

import edu.utah.med.genepi.gm.Gtype;
import edu.utah.med.genepi.gm.GtypeMatcher;
import edu.utah.med.genepi.gm.Ptype;
import edu.utah.med.genepi.hc.compressGtype;
import edu.utah.med.genepi.ped.Indiv;
import edu.utah.med.genepi.ped.PedData;
import edu.utah.med.genepi.ped.PedQuery;
import edu.utah.med.genepi.ped.Study;
import edu.utah.med.genepi.util.Counter;
import edu.utah.med.genepi.util.Ut;

//=============================================================================
public class CCAnalysis {

  public interface Table {
    public int getColumnCount();
    public Column getColumnAt(int index);
    public Row getRowAt(int index);
    public Row getRowFor(Ptype pt);
    public Totals getTotals();
    public String getTableName();
    public String getColumnHeading();
    public String getRowHeading();
    public String getColumnLabelAt(int index);
    public int getRowCount();
    public String getAttachMessage();
    
    public interface Column {
      public int subsumesAtype(Gtype gt, boolean first);
      public int subsumesGtype(Gtype gt);
      public int getWeight();
      //public String getType();
      public GtypeMatcher[] getGtypeMatcher();
    }
    public interface Row {
      public Ptype getPtype();
      public Number[] getCells();
      public String getLabel();
      public int[] getCellsN();
    }
    public interface Totals {
      public Number[] forColumns();
      public Number[] forRows();
      public Number forTable();
    }
  }
 
  public Study[]          study;
  private int             nstudy, nstats, nmetas, nmetasTDT, percent;
  private final String    myModel, myType;
  private final String[]  myTableType;
  private CCStat[]        ccstat, metastat, metaTDTstat;
  private CCStat          myStat = null;
  private final int[]     colWeights;
  private final int       iRefCol;
  private static int[]    quantids;
  private Indiv[][]       caseInds, controlInds, unknownInds;
  private Indiv[]         caseTDTInds, controlTDTInds, unknownTDTInds;
  private Table.Column[]  col_def;
  private Table[][][][]   observedTable;
  private Table[][][]     obsMetaTDTTable;
  private Table           statTable = null;
  private int[]           loci;
  private String          application_id;
  private int[][]         repeatGroup;
  private int             nrepeat;
  private int             ntabletype;

  //---------------------------------------------------------------------------
  //public CCAnalysis(int index, Table.Column[] column_defs, String[] markers,
  //public CCAnalysis(Table.Column[] column_defs, String[] markers,
  public CCAnalysis(Table.Column[] column_defs, 
                    String model, CCStat[] stats, CCStat[] metas,
                    String type, int[] quantIDs, int percentage, 
                    int[][] repeat, String[] tabletype, String app_id)
  {
    //myIndex = index;
    //myMarkers = markers;
    myModel = model;
    myType = type;
    myTableType = tabletype;
    ccstat = stats;
    nstats = stats.length;
    col_def = column_defs;
    percent = percentage;
    repeatGroup = repeat;
    nrepeat = repeatGroup.length;
    ntabletype = tabletype.length;
    application_id = app_id;
    if ( quantIDs != null )
      quantids = quantIDs;
    else 
      quantids = new int[1];
    colWeights = extractWeights(column_defs);
    iRefCol = Ut.indexOfMin(colWeights);
    // separate metaTDT from other meta analysis
    if ( metas.length > 0 ) {
      ArrayList<CCStat> metalist = new ArrayList<CCStat>();
      ArrayList<CCStat> metaTDTlist = new ArrayList<CCStat>();
      for (CCStat ms : metas ) {
        if ( (ms.getStatType()).equals("metaTDT") ) {
	  metaTDTlist.add(ms);
        }
	else { 
	  metalist.add(ms);
        }
      }
      if ( metalist.size() > 0 ) {
	metastat = (CCStat[]) metalist.toArray(new CCStatImp[0]);
        nmetas = metastat.length;
      }
      if ( metaTDTlist.size() > 0 ) {
	metaTDTstat = (CCStat[]) metaTDTlist.toArray(new CCStatImp[0]);
        nmetasTDT = metaTDTstat.length;
      }
    }
  }

  //---------------------------------------------------------------------------
  public int[] getLoci() 
  {
    GtypeMatcher[] gtm = col_def[0].getGtypeMatcher();
    return gtm[0].iLoci;
  }
    
  public int[] getLoci(int index) 
  {
    return repeatGroup[index];
  }
  
  public int[] getiLoci(int index) 
  {
    return loci;
  }

  //---------------------------------------------------------------------------
  //public int getIndex() { return myIndex; }

  //---------------------------------------------------------------------------
  //Use getLoci and then convert it back to MarkerName thru' GDef
  //public String[] getMarkers() { return myMarkers; }

  //---------------------------------------------------------------------------
  public String getModel() { return myModel; }

  //---------------------------------------------------------------------------
  public String getType() { return myType; }

  //---------------------------------------------------------------------------
  public String[] getTableType() { return myTableType; }

  //---------------------------------------------------------------------------
  public int[] getColumnWeights() { return colWeights; }

  //---------------------------------------------------------------------------
  public int getReferenceColumnIndex() { return iRefCol; }

  //---------------------------------------------------------------------------
  //public Table[][] getTables() { return ltable; }

  //---------------------------------------------------------------------------
  public Table[][][][] getObservedTable() { return observedTable; }

  //---------------------------------------------------------------------------
  public Table[][][] getObsMetaTDTTable() { return obsMetaTDTTable; }

  //---------------------------------------------------------------------------
  //public Table getTrioTDTTable() { return trioTDTTable; }
 
  /*
  public void startObsRuns( int sigrun, int simcycle, compressGtype[] cGtype,
                            int analysis_index )
  {
    CCStatRun[][][] statruns = this.statRuns;
    observedTable = new Table[nrepeat][nstudy][];
    String datatype = "sim";
    //If simcycle is -1 then always use "obs"
    if( simcycle == -1 )
    {
      datatype = "obs";
    }
    
    for ( int r = 0; r < nrepeat; r++ )
    {
      if ( nrepeat > 1 )
      {
        for ( int c = 0; c < col_def.length; c++ )
        {
          GtypeMatcher[] gtm = col_def[c].getGtypeMatcher();
          
          for ( int g = 0; g < gtm.length; g++ )
          {
            gtm[g].updateSelectedLocus(repeatGroup[r]);
          }
        }
      }
      for ( int s = 0; s < nstudy; s++ )
      {
        ArrayList<Table> Ltable = new ArrayList<Table>();
        loci = getLoci();
        //boolean failed = false;
        //List failed_tests = new ArrayList();
        for (int i = 0; i < nstats; ++i)
        {
          myStat = statRuns[r][s][i].myStat;
          statTable = (Table) crazyAces(sigrun,simcycle,datatype,cGtype);
          boolean addtabletolist = true;
          if (statTable != null )
          {
            if (i > 0)
              addtabletolist = false;
            if ( addtabletolist )
              Ltable.add(statTable);
          }
          statRuns[r][s][i].computeObserved(statTable);
          //String result = statRuns[r][s][i].getObservationalReport();
          String result = statRuns[r][s][i].observedResult.toString();
          String name = statRuns[r][s][i].getStatName();
          String [] results = result.split(":");
          String obsvalue = "";
          if( results.length > 1 )
          {
            if( results[0].equals("WARNING") )
            {
              obsvalue = "-1";
            }
            else
            {
              obsvalue = results[1];
            }
          }
          else
          {
            obsvalue = results[0];
          }
          if( obsvalue.equals("-") )
          {
            obsvalue = "-1";
          }
          if( obsvalue.equals("-1") )
          {
            String val = analysis_index + ":" + i;
            cGtype.store_badanalyses_indices(val);
          }
        }
        observedTable[r][s] = (Table[]) Ltable.toArray(new Table[0]);
      }
    }
  }

  //----------------------------------------------------------
  public void startsimRuns ( int sigrun, int simcycle,
                             compressGtype cGtype,
                             int analysis_index)
  {
    //System.out.println("Startsimruns Thread: " + p.getId());
    //added extra loop for repeat runs
    for ( int r = 0; r < nrepeat; r++ )
    {
      if ( nrepeat > 1 )
      {
        for ( int c = 0; c < col_def.length; c++ )
        {
          GtypeMatcher[] gtm = col_def[c].getGtypeMatcher();
          for ( int g = 0; g < gtm.length; g++ )
            gtm[g].updateSelectedLocus(repeatGroup[r]);
        }
      }
      for ( int s = 0; s < nstudy; s++ )
      { 
        ArrayList<Table> Ltable = new ArrayList<Table>();
        loci = getLoci();
        for (int i = 0; i < nstats; ++i)
        {
          if(!cGtype.check_bad_analyses(analysis_index, i))
          {
            myStat = statRuns[r][s][i].myStat;
            //System.out.println("Started Analysis: " + this.myModel);
            Table t = (Table) crazyAces(sigrun,simcycle,"sim",cGtype);
            statRuns[r][s][i].computeSimulated(t);
          }
        }
      //System.out.println("Ended Analysis: " + this.myModel);
      }
    }
  }
  **/

  //---------------------------------------------------------------------------
  public int getRepeat()
  {
    return nrepeat;
  }

  //---------------------------------------------------------------------------
  /*private void tallyExpressionEvents(Indiv.GtSelector gs)
  {
    gpTallier.resetTallies();
    for (int i = 0; i < caseInds.length; ++i)
      gpTallier.countExpressionEvent(
        caseInds[i].getGtype(gs), Ptype.CASE
      );
    for (int i = 0; i < controlInds.length; ++i)
      gpTallier.countExpressionEvent(
        controlInds[i].getGtype(gs), Ptype.CONTROL
      );
  }

  //---------------------------------------------------------------------------
  private void tallyQuantExpEvents(Indiv.GtSelector gs)
  {
    quantTallier.resetTallies();
    for (int i = 0; i < caseInds.length; ++i)
    {
      for ( int j = 0 ; j < quantids.length; j++ )
      {
        //if ( (caseInds[i].quant_val).getQdata(j) != 0.0  )
        if ( caseInds[i].quant_val != null )
          quantTallier.sumExpressionEvent(
            caseInds[i].getGtype(gs), j, 
            (caseInds[i].quant_val).getQdata(j) );
      }
    }
    for (int i = 0; i < controlInds.length; ++i)
    {
      for ( int j = 0 ; j < quantids.length; j++ )
      {
        if  ( controlInds[i].quant_val != null )
        quantTallier.sumExpressionEvent(
          		controlInds[i].getGtype(gs), j,
			(controlInds[i].quant_val).getQdata(j) );
      }
    }
  }
  **/
  //---------------------------------------------------------------------------
  private static int[] extractWeights(Table.Column[] cols)
  {
    int[] wts = new int[cols.length];
    for (int i = 0; i < cols.length; ++i)
      wts[i] = cols[i].getWeight();
    return wts;
  }
  //---------------------------------------------------------------------------
  public boolean checkDistinctWeights()
  {
    int[] wt = getColumnWeights();
    ArrayList<Integer> wtlist = new ArrayList<Integer>();
    for ( int i = 0; i < wt.length - 1; i++)
    {
      //System.out.println("wt at " + i + " is " + wt[i]);
      //System.out.println( "******************" );
      //for ( Iterator it = wtlist.iterator(); it.hasNext(); )
      //  System.out.println("list has : " + it.next() );
      if ( wtlist.contains(new Integer(wt[i])))
        return false;
      else 
      {
        wtlist.add(new Integer(wt[i]));
        //System.out.println("add : " + wt[i] + " to list");
      }
    }
    return true;
  }

  //---------------------------------------------------------------------------
  public boolean checkDistinctRefWt()
  {
    int flag = 0;
    for ( int i = 0; i < colWeights.length; i++)
    {
      if ( colWeights[i] == colWeights[iRefCol] )
        flag++;
      if ( flag > 1 )
        return false;
    }
    return true;
  }

  //----------------------------------------------------------------------------
  // added 2-25-08 Ryan Abo
  private Table fill_counters(String model, compressGtype cGtype, int hap)
  {
	Ptype[] thePtypes = new Ptype[] {Ptype.CASE, Ptype.CONTROL};
	int numcols = col_def.length;
	int numrows = thePtypes.length;
	int[] pid2RowIx = {0,1,0};
	int[][] vals = null;
	
    SortedSet store = (SortedSet) new TreeSet();
	  
    for( int i=0; i < this.loci.length; i++ )
    {
      store.add(this.loci[i] + 1);
    }
    
	if(model.equals("M"))
	{
      vals = cGtype.loci_monotype_prescreens.get(store.toString());
	}
	else if(model.equals("A"))
	{
      int[][] hets = cGtype.loci_diphet_prescreens.get(store.toString());
	  int[][] homs = cGtype.loci_diphom_prescreens.get(store.toString());
	  int[] case_cells = {homs[0][hap],hets[0][hap],cGtype.num_cases};
	  case_cells[2] = case_cells[2] - case_cells[0] - case_cells[1];
	  int[] control_cells = {homs[1][hap],hets[1][hap],cGtype.num_controls};
	  control_cells[2] = control_cells[2] - control_cells[0] - control_cells[1];
	  int[][] v = {case_cells, control_cells};
	  vals = v;
	}
	else if(model.equals("D"))
	{
      int[][] hets = cGtype.loci_diphet_prescreens.get(store.toString());
	  int[][] homs = cGtype.loci_diphom_prescreens.get(store.toString());
	  int[] case_cells = {homs[0][hap] + hets[0][hap], cGtype.num_cases};
	  case_cells[1] = case_cells[1] - case_cells[0];
	  int[] control_cells = {homs[1][hap] + hets[1][hap], cGtype.num_controls};
	  control_cells[1] = control_cells[1] - control_cells[0];
	  int[][] v = {case_cells, control_cells};
	  vals = v;    
	}
	else if(model.equals("R"))
	{
      int[][] hets = cGtype.loci_diphet_prescreens.get(store.toString());
	  int[][] homs = cGtype.loci_diphom_prescreens.get(store.toString());
	  int[] case_cells = {homs[0][hap], cGtype.num_cases};
	  case_cells[1] = case_cells[1] - case_cells[0];
	  int[] control_cells = {homs[1][hap], cGtype.num_controls};
	  control_cells[1] = control_cells[1] - control_cells[0];
	  int[][] v = {case_cells, control_cells};
	  vals = v;    
	}	
	Counter[][] theCounters = set_counters(numrows, numcols, vals);
	return null;// new ContingencyTable(thePtypes, col_def, theCounters, pid2RowIx);
  }
 
  //----------------------------------------------------------------------------
  private Counter[][] set_counters(int nRows, int nCols, int[][] vals)
  {
    Counter[][] counters = new Counter[nRows][nCols];
    for ( int irow = 0; irow < nRows; irow++ )
    {
      for ( int icol = 0; icol < nCols; icol++ )
      {
        counters[irow][icol] = new Counter();
        counters[irow][icol].set(vals[irow][icol]);
      }
    }
    return counters;     
  }
}
