//******************************************************************************
// XTopSim.java
//******************************************************************************
package edu.utah.med.genepi.sim;

import edu.utah.med.genepi.gm.AllelePair;
import edu.utah.med.genepi.gm.FreqDataSet;
import edu.utah.med.genepi.gm.GDef;
import edu.utah.med.genepi.gm.Gtype;
import edu.utah.med.genepi.hc.compressGtype;
import edu.utah.med.genepi.ped.Indiv;
import edu.utah.med.genepi.ped.PedQuery;
import edu.utah.med.genepi.util.EmpiricalRandomizer;
import edu.utah.med.genepi.util.GEException;
import edu.utah.med.genepi.util.Randy;

//==============================================================================
public class XTopSim extends AlleleFreqTopSim {

  public XTopSim()
  { super(); }

  // overloaded for hapBuilder
  public void simulateFounderGenotypes( int index, 
                                        compressGtype cGtype,
                                        int step) throws GEException
  {
    System.out.println("WARNING: simulated founders without using compressGtype");
  }

  //----------------------------------------------------------------------------
  public EmpiricalRandomizer[] getAlleleMap(int studyID)
  {
    return randAlleleAt[studyID];
  }
  //----------------------------------------------------------------------------
  //public static Gtype[] getPedGtype()
  //{  return pedGtype; }
}

