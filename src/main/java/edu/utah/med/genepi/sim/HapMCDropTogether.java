//******************************************************************************
// HapMCDropTogether.java
//******************************************************************************
package edu.utah.med.genepi.sim;

import edu.utah.med.genepi.hc.compressGtype;
import edu.utah.med.genepi.ped.PedQuery;
import edu.utah.med.genepi.util.GEException;

//==============================================================================
public class HapMCDropTogether extends HapMCDropSim 
{
  PedQuery.Predicate[] querySample; 
  
  //----------------------------------------------------------------------------
  public HapMCDropTogether()
  {
    super();
    //querySample = new PedQuery.Predicate[] {PedQuery.IS_ANY};
    querySample = new PedQuery.Predicate[] {PedQuery.IS_TOGETHER};
  }
}
