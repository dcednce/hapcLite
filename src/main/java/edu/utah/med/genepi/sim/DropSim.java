//******************************************************************************
// DropSim.java
//******************************************************************************
package edu.utah.med.genepi.sim;

import java.util.Iterator;
import java.util.List;

import edu.utah.med.genepi.gm.AllelePair;
import edu.utah.med.genepi.gm.GDef;
import edu.utah.med.genepi.gm.Gamete;
import edu.utah.med.genepi.gm.Gtype;
import edu.utah.med.genepi.gm.GtypeBuilder;
import edu.utah.med.genepi.hc.compressGtype;
import edu.utah.med.genepi.ped.Indiv;
import edu.utah.med.genepi.ped.Marriage;
import edu.utah.med.genepi.ped.PedData;
import edu.utah.med.genepi.ped.PedQuery;
import edu.utah.med.genepi.ped.Pedigree;
import edu.utah.med.genepi.ped.Study;
import edu.utah.med.genepi.util.GEException;

//==============================================================================
public class DropSim implements GSimulator.Drop {

  protected Study[]      study;
  protected GDef         gdef;
  protected Indiv.GtSelector gtOBS, gtSIM;
  protected Pedigree[][] thePeds;
  protected Indiv[][]    descendInds, anyInds;
  public int           nStudy, nLoci;
  public int           nSim;
  protected int[]        nDescendants, nPeds;
  protected GtypeBuilder gtBuilder;
  public byte 	       missingData;

  //----------------------------------------------------------------------------
  public void preProcessor () throws GEException
  {}

  //----------------------------------------------------------------------------
  public void setPedData()
  {
    descendInds  = new Indiv[nStudy][];
    anyInds      = new Indiv[nStudy][];
    thePeds      = new Pedigree[nStudy][];
    nPeds        = new int[nStudy];
    nDescendants = new int[nStudy];
  
    for ( int i = 0; i < nStudy; i++ )
    {
      PedData pd = study[i].getPedData();
      thePeds[i] = pd.getPedigrees();
      nPeds[i] = thePeds[i].length;
      descendInds[i] = pd.getIndividuals(PedQuery.IS_DESCENDANT);
      anyInds[i] = pd.getIndividuals(PedQuery.IS_ANY);
      nDescendants[i] = descendInds[i].length;
    }
  }

  //----------------------------------------------------------------------------
  public void setGDef(GDef gd)
  {
    nLoci = gd.getLocusCount();
    gtBuilder = gd.getGtypeBuilder();
    gdef = gd;
    missingData = gd.getAlleleFormat().getMissingData();
  }

  //----------------------------------------------------------------------------
  //public void setNumSimulation( int num )
  //{
  //  nSim = num;
  //}

  // overloaded for hapBuilder
  public void simulateDescendantGenotypes(int index, compressGtype[] cGtype,
                                          int step)
  throws GEException
  {
    System.out.println("WARNING : simulated descendant without using compressGtype");
  }
}
