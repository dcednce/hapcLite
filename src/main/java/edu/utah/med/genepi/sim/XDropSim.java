//******************************************************************************
// XDropSim.java
//******************************************************************************
package edu.utah.med.genepi.sim;

import java.util.Iterator;
import java.util.List;

import edu.utah.med.genepi.gm.AllelePair;
import edu.utah.med.genepi.gm.Gamete;
import edu.utah.med.genepi.gm.Gtype;
import edu.utah.med.genepi.ped.Indiv;
import edu.utah.med.genepi.ped.Marriage;
import edu.utah.med.genepi.util.GEException;
//import edu.utah.med.genepi.util.Ut;

//==============================================================================
public class XDropSim extends DropSim
{
  public XDropSim ()
  {
    super();
  }
}
