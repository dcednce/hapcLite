//******************************************************************************
// HapFreqTopSim.java
//******************************************************************************
package edu.utah.med.genepi.sim;

import java.io.IOException;

import edu.utah.med.genepi.gm.FreqDataSet;
import edu.utah.med.genepi.gm.Gtype;
import edu.utah.med.genepi.io.HapDataLoader;
import edu.utah.med.genepi.util.GEException;
import edu.utah.med.genepi.util.Randy;

//==============================================================================
public class HapFreqTopSim extends AlleleFreqTopSim 
{
  protected String[] 		code;
  protected FreqDataSet[] 	newcumfreq; 
  public static String[] defaultnames;
  public static double[] defaultcumfreq;

  //----------------------------------------------------------------------------
  public HapFreqTopSim ()
  { 
    super();
  }
  //----------------------------------------------------------------------------
  public void preProcessor () throws GEException
  {
    for ( int i = 0; i < study.length; i++ )
    {
      HapDataLoader loader = new HapDataLoader();
      try 
      { loader.parse( study[i].getHaplotypeFile(), gdef.getAlleleFormat() ); }
      catch ( IOException e )
      { throw new GEException (e.getMessage()); }

      FreqDataSet[] freqdata = loader.getHapFreq();
      study[i].setFreqDataSet(freqdata);
    }
  }

  
  
  

  //---------------------------------------------------------------------
  //public static void setcumfreq(double[] cf)
  //{ defaultcumfreq = cf; }

  //---------------------------------------------------------------------
  //public static void setnames(String[] n)
  //{ defaultnames = n; }

  //---------------------------------------------------------------------
  public FreqDataSet[] getcumfreq (int studyID)
    {	
      FreqDataSet[][] freq = study[studyID].getFreqDataSet();
      FreqDataSet[] cumfreq = new FreqDataSet[freq[0].length];
      cumfreq[0] = freq[0][0];
      for ( int i = 1; i < cumfreq.length; i++ )
        cumfreq[i] = new FreqDataSet( cumfreq[i - 1].getFrequency() +
                                       freq[0][i].getFrequency(), 
                                      freq[0][i].getCode(),
                                      "HapFreqTopSim" );
        
      return cumfreq; 
    }

  //---------------------------------------------------------------------
/**  public void getdbl()
  {
    Randy a = Randy.getInstance();
    double randomNum = a.nextDouble();
  }
*/
}

