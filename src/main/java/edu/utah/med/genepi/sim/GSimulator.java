//******************************************************************************
// GSimulator.java
//******************************************************************************
package edu.utah.med.genepi.sim;

import edu.utah.med.genepi.gm.GDef;
import edu.utah.med.genepi.hc.compressGtype;
import edu.utah.med.genepi.ped.Study;
import edu.utah.med.genepi.util.GEException;

//==============================================================================
public interface GSimulator {
  public void setPedData();
  public interface Top extends GSimulator {
    public void simulateFounderGenotypes(int index, compressGtype cGtype, int step) throws GEException;
  }
  public interface Drop extends GSimulator {
  }
}
